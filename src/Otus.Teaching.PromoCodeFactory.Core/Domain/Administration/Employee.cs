﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Работник - менеджер  
    /// </summary>
    public class Employee : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Эл.почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Количество использованных промокодов
        /// </summary>
        public int AppliedPromoCodesCount { get; set; }

        /// <summary>
        /// Роль менеджера партнёра 
        /// </summary>
        public Role Role { get; set; }

        public Guid RoleId { get; set; }
        public IEnumerable<PromoCode> PromoCodes { get; set; }
        public string FullName => $"{LastName} {FirstName}";
    }
}