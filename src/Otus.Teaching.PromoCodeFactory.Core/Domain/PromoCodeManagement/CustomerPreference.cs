﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// сущность, связывающая клиентов с его предпочтениями
    /// </summary>
    public class CustomerPreference : BaseEntity
    {
        /// <summary>
        /// Код клиента
        /// </summary>
        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }
        
        /// <summary>
        /// Предпочтение клиента
        /// </summary>
        public Guid PreferenceId { get; set; }

        public Preference Preference { get; set; }
    }
}