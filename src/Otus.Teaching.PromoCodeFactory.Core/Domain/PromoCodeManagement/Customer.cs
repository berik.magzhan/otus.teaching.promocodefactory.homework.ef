﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer
        : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        [StringLength(80)]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Эл.почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public virtual List<CustomerPreference> CustomerPreferences { get; set; }

        /// <summary>
        /// Промокоды
        /// </summary>
        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}