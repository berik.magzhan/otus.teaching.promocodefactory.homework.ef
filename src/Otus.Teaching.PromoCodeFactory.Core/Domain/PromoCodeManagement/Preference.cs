﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        /// <summary>
        /// предпочтения
        /// </summary>
        public string Name { get; set; }

        public IEnumerable<CustomerPreference> CustomerPreferences { get; set; }
        public IEnumerable<PromoCode> PromoCodes { get; set; }
    }
}