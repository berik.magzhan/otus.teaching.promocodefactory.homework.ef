﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Model;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly PromoDbContext _promoDbContext;

        public EfRepository(PromoDbContext promoDbContext)
        {
            _promoDbContext = promoDbContext;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _promoDbContext.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _promoDbContext
                .Set<TEntity>()
                .FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<TEntity> FirstOrDefaultWithInclude(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] children)
        {
            IQueryable<TEntity> set = _promoDbContext.Set<TEntity>();
            foreach (var child in children)
            {
                set = set.Include(child);
            }

            var entity = await set.FirstOrDefaultAsync(predicate);
            return entity;
        }
        
        public async Task CreateAsync(TEntity entity)
        {
            await _promoDbContext.Set<TEntity>().AddAsync(entity);
            await _promoDbContext.SaveChangesAsync();
        }
        
        public async Task UpdateAsync(TEntity entity)
        {
            await _promoDbContext.SaveChangesAsync();
        }
        
        public async Task DeleteByIdAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            _promoDbContext.Remove(entity);
            await _promoDbContext.SaveChangesAsync();
        }
    }
}