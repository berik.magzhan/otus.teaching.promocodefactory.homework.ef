﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Model;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private PromoDbContext _context { get; set; }

        public DbInitializer(PromoDbContext context)
        {
            _context = context;
        }

        public void Init()
        {
            _context.Database.EnsureCreated();
        }
    }
}
