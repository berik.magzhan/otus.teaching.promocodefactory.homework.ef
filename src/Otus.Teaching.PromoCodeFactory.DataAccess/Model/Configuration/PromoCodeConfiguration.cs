﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Model.Configuration
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder
                .Property(e => e.PartnerManagerId).HasMaxLength(200);
            builder
                .HasOne(pc => pc.PartnerManager)
                .WithMany(p => p.PromoCodes)
                .HasForeignKey(pc => pc.PartnerManagerId);
            builder
                .HasOne(pc => pc.Preference)
                .WithMany(p => p.PromoCodes)
                .HasForeignKey(pc => pc.PreferenceId);
        }
    }
}