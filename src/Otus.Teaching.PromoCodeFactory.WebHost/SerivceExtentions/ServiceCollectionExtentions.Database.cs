using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Model;

namespace SmartBuilding.MobileApi.Extensions.Infrastructure
{
    /// <summary>
    /// Миграция БД
    /// </summary>
    public static class ServiceCollectionExtensionsDatabase
    {
        /// <summary>
        /// Миграция БД. Вызвать перед первым обращением к БД
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static IServiceCollection MigrateDataBase(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();

            var dbContext = serviceProvider.GetService<PromoDbContext>();
            if (dbContext == null)
                throw new NullReferenceException($"Can not resolve DbContext");

            dbContext.Database.Migrate();

            return services;
        }
    }
}