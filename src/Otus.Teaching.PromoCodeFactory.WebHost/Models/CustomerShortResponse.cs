﻿using System;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }

    internal class CustomerShortResponse_Map_Profile : Profile
    {
        public CustomerShortResponse_Map_Profile()
        {
            CreateMap<Customer, CustomerShortResponse>();
        }
    }
}