﻿using System;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Предпочтение
    /// </summary>
    public class PreferenceResponse
    {
        /// <summary>
        /// Идентификатор предпочтения
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Наименование предпочтения
        /// </summary>
        public string Name { get; set; }
    }
    
    internal class PreferenceResponse_Map_Profile : Profile
    {
        public PreferenceResponse_Map_Profile()
        {
            CreateMap<Preference, PreferenceResponse>();
        }
    }
}