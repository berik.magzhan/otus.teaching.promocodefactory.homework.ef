﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<PreferenceResponse> Preferences { get; set; }
        
        public List<PromoCodeShortResponse> PromoCodes { get; set; }
    }
    
    internal class CustomerResponse_Map_Profile : Profile
    {
        public CustomerResponse_Map_Profile()
        {
            CreateMap<Customer, CustomerResponse>();
        }
    }
}